
/*****************************************************************************************************
 *
 * Author: Kiran Hegde
 * Date: 09/22/2018
 * Tools: Vim editor and GCC compiler
 * 
 * File: common.h
 * This file contains the message structure and acts as data structure for client server communication.
 * Also defines the size of the strcture 
 *
 ***************************************************************************************************/

#ifndef COMMON_H_

#define COMMON_H_

#define BUFSIZE (1024)

/* structure for message containing sequence number, buffer and acknowledgement */
typedef struct message
{
	char buff[BUFSIZE];
	unsigned int seqno;
	unsigned int ACK;
	//unsigned int NACK;
	unsigned int read_bytes;
}message;

#define MSGSIZE (sizeof(struct message))
#endif

/*****************************************************************************************************
 *
 * End of File 
 *
 ***************************************************************************************************/
