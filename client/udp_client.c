/*****************************************************************************************************
 *
 * Author: Kiran Hegde
 * Some of the code is from Sangtae Ha's example client-server code
 * Date: 09/22/2018
 * Tools: Vim editor and GCC compiler
 * 
 * File: udp_client.c
 * Implements UDP communication between server and client systems with some reliability
 *
 * udpclient.c - A simple UDP client
 * usage: udpclient <host> <port>
 *
 ***************************************************************************************************/

/****************************************************************************************************
*
* Header Files
*
****************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include "common.h"

/****************************************************************************************************
 * 
 * error - wrapper for perror
 *
 ***************************************************************************************************/
void error(char *msg) 
{
	perror(msg);
	exit(0);
}

/****************************************************************************************************
*
* @name GetFile
* @brief Receive file from Server
*
* This function receives the mentioned file from the server. Creates a new file 
* appending _rcvd with the original name. 
*
* @param sockect instance
* @param buf - original file name 
* @param serveraddr - of type struct sockaddr_in
* @param serverlen 
*
* @return 0 on No error and 1 for file not found error
*
****************************************************************************************************/

int GetFile(int sockfd, char *buf, struct sockaddr_in serveraddr, int serverlen)
{
        int m=0, n=0;
        int kk;
	message msg;
	int prev_seqno = 0;
        FILE * f_ptr=NULL;
	sprintf(msg.buff, "%d", n);
	/* receive the size of the file */
	while(n<1)
		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
	kk = atoi(msg.buff);
	prev_seqno = msg.seqno;
	msg.ACK = 1;
	n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
	/* check if file exists or not based on received value */
	if(kk == -10)
		return 1;
        n = 0;
	/* Append the name with _recvd */
	strcat(buf, "_recvd");
	/* Open the file for writing */
	f_ptr = fopen(buf, "w");
	/* while size remaining to be received is greater than zero */
        while(kk > 0)
        {
                bzero(&msg.buff, BUFSIZE);
                m = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
		if(m>0)
		{
			/* check if received packet is next or the same packet as previous */
			if(msg.seqno > prev_seqno)
			{
				printf("SeqNo: %d\n", msg.seqno);
				prev_seqno = msg.seqno;
                		kk -= msg.read_bytes;
                		n = fwrite(&msg.buff, 1, msg.read_bytes, f_ptr);
			}
			else
				printf("Packet dropped SeqNo: %d\n", msg.seqno);
			msg.ACK = 1;
			/* send acknowledgement */
                	n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
		}
        }
	/* close the file handler */
        fclose(f_ptr);
	return 0;
}

/****************************************************************************************************
*
* @name PutFile
* @brief Send file to Server
*
* This function sends the mentioned file to the server. 
*
* @param sockect instance
* @param buf - original file name 
* @param serveraddr - of type struct sockaddr_in
* @param serverlen 
*
* @return 0 on No error and 1 for file not found error
*
****************************************************************************************************/
int PutFile(int sockfd, char *buf, struct sockaddr_in serveraddr, int serverlen)
{
	int n;
	FILE *file_ptr;
	message msg;
	/* check if file is present or not */
	msg.ACK = 0;
	unsigned int count = 0;
	if(!(file_ptr = fopen(buf, "r")))
	{
		printf("file not found\n");
		strcpy(msg.buff, "-10");
		while(msg.ACK == 0 && count <3)
		{
			msg.ACK = 0;
			n = sendto(sockfd, &msg, MSGSIZE, 0, 
						(struct sockaddr *)&serveraddr, serverlen);
			n = recvfrom(sockfd, &msg, MSGSIZE, 0, 
						(struct sockaddr *)&serveraddr, &serverlen);
			count++;
		}
		return 1;
	}
	int read_size;
        int sz;
	/* get the file size */
        fseek(file_ptr, 0L, SEEK_END);
        sz = ftell(file_ptr);
        rewind(file_ptr);
	msg.seqno = 1;
	sprintf(msg.buff, "%d", sz);
	/* send file size that has to be sent */
	while(msg.ACK == 0 && count <3)
	{
		msg.ACK = 0;
		count++;
		n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
	}
	bzero(&msg.buff, BUFSIZE);
	msg.ACK = 0;
	int rep=0;
	msg.seqno++;
	/* read the file into 1024 byte buffer and send it to server */
	while((read_size=fread(&msg.buff, 1, BUFSIZE, file_ptr))>=BUFSIZE)
	{
		msg.read_bytes = read_size;
		while(msg.ACK == 0)
		{
			msg.ACK = 0;
			/* check if packet is dropped by checking the timeout */
			if(rep!=0)
				printf("Packet Dropped with SeqNo: %d\n", msg.seqno);
			n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
			n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
			rep++;
		}
		rep=0;
		msg.ACK = 0;
		msg.read_bytes = 0;
		/* increase the sequence number for next packet */
		msg.seqno++;
	}
	msg.ACK = 0;
	msg.read_bytes = read_size;
	fclose(file_ptr);
	/* Certain last bytes of data */
	while(msg.ACK == 0)
	{
		msg.ACK = 0;
		n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
	}
	return 0;
}

/****************************************************************************************************
*
* @name FileDelete
* @brief Delete a file in the server
*
* This function deletes mentioned file in the server. 
*
* @param sockect instance
* @param buf - original file name 
* @param serveraddr - of type struct sockaddr_in
* @param serverlen 
*
* @return 0 on No error and 1 for file not found error
*
****************************************************************************************************/
int FileDelete(int sockfd, char *buf, struct sockaddr_in serveraddr, int serverlen)
{
        message msg;
	int m=0, n;
        int kk=0;
        FILE * f_ptr=NULL;
	/* server sends 5 on success and -10 for failure */
	while(kk != 5 && kk != -10)
	{
        	n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
		msg.ACK = 1;
		/* convert string in buffer to integer */
		kk = atoi(msg.buff);
        	n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
	}
	if(kk == -10)
        {
                printf("file not found\n");
                return 1;
        }
	printf("file deleted\n");
	return 0;
}

/****************************************************************************************************
*
* @name FileNames
* @brief List the files of a given directory
*
* This function lists out the names of all the files present in a directory . 
*
* @param sockect instance
* @param serveraddr - of type struct sockaddr_in
* @param serverlen 
*
* @return 0 on No error
*
****************************************************************************************************/
int FileNames(int sockfd, struct sockaddr_in serveraddr, int serverlen)
{
	int m=0, n=0;
        int kk;
        message msg;
	/* wait till the data is received */
	while(n<1)
        	n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
        kk = atoi(msg.buff);
	msg.ACK =1;
	n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
        n = 0;
        int prev_seqno=0;
	/* until all the names are received */
        while(kk > 0)
        {
                bzero(&msg.buff, BUFSIZE);
                m = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
                if(m>0)
                {
			/* check if packet recived is next one or the previous. If previos, it is discarded */
                        if(msg.seqno > prev_seqno)
                        {
                                prev_seqno = msg.seqno;
                                kk -= msg.read_bytes;
				printf("%s", msg.buff);
                        }
                        msg.ACK = 1;
			/* send the acknowledgement */
                        n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
                }
        }
        return 0;
}

/****************************************************************************************************
*
* @name Main
* @brief Implements the client functionality 
*
* This is the main function where all the client functionalities are implemented. 
*
* @param argc - argument count 
* @param **argv - pointer to argument  
*
* @return 0 on No error
*
****************************************************************************************************/
int main(int argc, char **argv) 
{
	int sockfd, portno, n=0;
	int serverlen;
	struct sockaddr_in serveraddr;
	struct hostent *server;
	char *hostname;
	char buf[BUFSIZE];

	/* check command line arguments */
	if (argc != 3) 
	{
		fprintf(stderr,"usage: %s <hostname> <port>\n", argv[0]);
		exit(0);
	}
	message msgg;
	hostname = argv[1];
	portno = atoi(argv[2]);
	
	/* socket: create the socket */
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) 
		error("ERROR opening socket");

	/* setsockopt: Handy debugging trick that lets 
        * us rerun the server immediately after we kill it; 
        * otherwise we have to wait about 20 secs. 
        * Eliminates "ERROR on binding: Address already in use" error. 
	* Also, sets timeout time
        */
        struct timeval tv;
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv , sizeof(tv));

	/* gethostbyname: get the server's DNS entry */
	server = gethostbyname(hostname);
	if (server == NULL) 
	{
		fprintf(stderr,"ERROR, no such host as %s\n", hostname);
		exit(0);
	}

	/* build the server's Internet address */
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
	serveraddr.sin_port = htons(portno);
	serverlen = sizeof(serveraddr);
	unsigned int quit = 0;
	/* get a message from the user */
	while(!quit)
	{
		bzero(&msgg, MSGSIZE);
		bzero(buf, BUFSIZE);
		printf("\n\n************Enter Command***********\n\n");
		printf("* ls\n* exit\n* delete [filename]\n* get [filename]\n* put [filename]\n* quit\n\n");
		printf("\n************************************\n\n");
		printf("$ ");
		/* get the command from standard i/o*/
		gets(buf);
		if(!strcmp(buf, "quit"))
		{
			quit = 1;
			continue;
		}
		strcpy(msgg.buff, buf);
		msgg.ACK = 0;
		msgg.seqno = 0;
		/* send the command to server */
		n = sendto(sockfd, &msgg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, serverlen);
		bzero(&msgg, MSGSIZE);
		/* get the acknowledgement */
		n = recvfrom(sockfd, &msgg, MSGSIZE, 0, (struct sockaddr *)&serveraddr, &serverlen);
		if(n<1)
		{
			printf("No reply from Server\n");
			continue;
		}
		strcpy(buf, msgg.buff);
		if(strncmp(buf, "get ", 4)==0)
		{
			int res;
			/* memmove to seperate the file name from command */
			memmove(buf, buf+4, strlen(buf)-4+1);
			if((res = GetFile(sockfd, buf, serveraddr, serverlen))==0)
				printf("File received\n");
			else
				printf("No file found\n");
		}
		else if(strncmp(buf, "put ", 4)==0)
		{
			int res;
			/* memmove to seperate the file name from command */
			memmove(buf, buf+4, strlen(buf)-4+1);
			res = PutFile(sockfd, buf, serveraddr, serverlen);
		}
		else if (strncmp(buf, "ls", 2)==0)
		{
			int res;
			res = FileNames(sockfd, serveraddr, serverlen);
		}
		else if (strncmp(buf, "delete ", 7)==0)
		{
			int res;
			res = FileDelete(sockfd, buf, serveraddr, serverlen);
		}
		else
			printf("%s\n", buf);
	}
		/* send the message to the server */
		return 0;
}

/****************************************************************************************************
*
* End of File
*
****************************************************************************************************/
