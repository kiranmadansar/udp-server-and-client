﻿Date: 09/23/2018
Programming Assignment 1 (CSCI 5273) - UDP Socket Programming
Submitted by Kiran Narayana Hegde


I have designed a UDP client and server systems, between which you can exchange data using simple commands. Since UDP doesn’t have any reliability built-in, I have implemented it on my own. Used recvfrom() function with a timeout value of 1 second. If acknowledgement for a particular packet is not received within the time by the transmitter,  respective packet is resent. On the receiver side, I am checking the sequence number of the packet and if the sequence number matches with that of the previous one, received packet is discarded. Hence, duplicate data packet processing will not happen. 


For transferring file, I am sending the file size first so that receiver will know how much data to receive. Once the received data matches with the file size, receiver will stop receiving and exit the loop. On transmitter side, I will read the file into 1024 bytes buffer and send it to receiver. This is continued until End of File is reached.  


I have verified my application for reliability by transferring 35 MB video and it works like a charm.  


How to build and use the application ?


1. There are two directories named ‘client’ and ‘server’ in the submitted .tar file. Untar it
2. Go to Client directory and use the following make command to build client application
$ make
1. Go to Server directory and use the following make command to build server application
$ make
1. In one terminal, run the server application using the following command
$ ./server <port_no>
Note: Port Number should be greater than 5000
1. In other terminal, run the client application using the following command
$ ./client <ip_address of server> <port_no>
Note: Use the same port number as server
1. A menu will be displayed about the useful commands. 
2. Use the command ‘exit’ (without space and all lower case) to safely close the server. Use the command ‘quit’ (without space and all lower case) to safely exit the client




Note: common.h is the header file used by both server and client application. It contains message structure for data transfer and size of that structure. Don’t delete it.
