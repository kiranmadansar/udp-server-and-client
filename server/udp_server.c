/*****************************************************************************************************
 *
 * Author: Kiran Hegde
 * Some of the code is from Sangtae Ha's example client-server code
 * Date: 09/22/2018
 * Tools: Vim editor and GCC compiler
 * 
 * File: udp_client.c
 * Implements UDP communication between server and client systems with some reliability
 * udpserver.c - A simple UDP echo server 
 * usage: udpserver <port>
 ***************************************************************************************************/

/****************************************************************************************************
*
* Header Files
*
****************************************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include "common.h"
#include <errno.h>

/****************************************************************************************************
 * 
 * error - wrapper for perror
 *
 ***************************************************************************************************/
void error(char *msg) 
{
	perror(msg);
	exit(1);
}

/****************************************************************************************************
*
* @name SendFile_Client
* @brief Send file to Client
*
* This function sends the mentioned file to the client. 
*
* @param sockect instance
* @param name - original file name 
* @param clientaddr - of type struct sockaddr_in
* @param clientlen 
*
* @return 0 on No error and 1 for file not found error
*
****************************************************************************************************/
int SendFile_Client(int sockfd, char *name, struct sockaddr_in clientaddr, int clientlen)
{
	int n;
	FILE *file_ptr;
	message msg;
	msg.ACK = 0;
	unsigned int count = 0;
	/* Check if file exists or not */
	if(!(file_ptr = fopen(name, "r")))
	{
		printf("File not found.\n");
		strcpy(msg.buff, "-10");
		while(msg.ACK == 0 && count <3)
		{
			msg.ACK = 0;
        		n = sendto(sockfd, &msg, MSGSIZE, 0,
                                                (struct sockaddr *)&clientaddr, clientlen);
			n = recvfrom(sockfd, &msg, MSGSIZE, 0, 
						(struct sockaddr *)&clientaddr, &clientlen);
                	count++;
		}
		return 1;
    	}
    	int read_size;
    	int sz;
	/* get file size */
    	fseek(file_ptr, 0L, SEEK_END);
    	sz = ftell(file_ptr);
    	rewind(file_ptr);
	msg.seqno = 1;
	sprintf(msg.buff, "%d", sz);
	/* send file size */
	while(msg.ACK == 0 && count <3)
	{
		msg.ACK = 0;
    		n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
		count++;
	}
	bzero(&msg.buff, BUFSIZE);
	msg.ACK = 0;
	int rep=0;
	msg.seqno++;
	/* read 1024 bytes of file into msg.buff */
    	while((read_size=fread(&msg.buff, 1, BUFSIZE, file_ptr))>=BUFSIZE)
    	{
		msg.read_bytes = read_size;
		while(msg.ACK == 0)
		{
			msg.ACK =0;
			/* check if timeout happens, if yes, check the packet drop */
			if(rep!=0)
				printf("Packet Dropped with SeqNo: %d\n", msg.seqno);
        		n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
        		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
			rep++;
		}
		rep=0;
		msg.ACK = 0;
		msg.read_bytes = 0;
		msg.seqno++;
	}
	fclose(file_ptr);
	msg.ACK = 0;
	msg.read_bytes = read_size;
	/* Send last bytes of file to client (size will be less than 1024 bytes) */
	while(msg.ACK == 0)
	{
		msg.ACK = 0;
    		n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
    		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
    	}
    	return 0;
}

/****************************************************************************************************
*
* @name ReceiveFile_Client
* @brief Receive file from Client 
*
* This function receives the mentioned file from client. Creates a new file 
* appending _rcvdfrmclient with the original name. 
*
* @param sockect instance
* @param name - original file name 
* @param clientaddr - of type struct sockaddr_in
* @param clientlen 
*
* @return 0 on No error and 1 for file not found error
*
****************************************************************************************************/
int ReceiveFile_Client(int sockfd, char *name, struct sockaddr_in clientaddr, int clientlen)
{
        int m=0, n=0;
        int kk;
	message msg;
        FILE * f_ptr=NULL;
	int prev_seqno = 0;
	/* wait for file size to be received */
	while(n<1)
		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
	kk = atoi(msg.buff);
	prev_seqno = msg.seqno;
	msg.ACK = 1;
	n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
	if(kk == -10)
		return 1;
	strcat(name, "_recvdfrmclient");
	f_ptr = fopen(name, "w");
	n = 0;
	/* until the last bytes is received */
        while(kk > 0)
        {
                bzero(&msg.buff, BUFSIZE);
		m = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
		/* Only if packet is received. Else, don't even send ACK*/
		if(m>0)
		{
			/* check if packet received is expected packet or not */
			if(msg.seqno > prev_seqno)
			{
				kk -= msg.read_bytes;
				printf("SeqNo: %d\n", msg.seqno);
				prev_seqno = msg.seqno;
				n = fwrite(&msg.buff, 1, msg.read_bytes, f_ptr);
			}
			else
				printf("Packet dropped SeqNo: %d\n", msg.seqno);
			msg.ACK = 1;
			/* send the acknowledgement */
        		n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
		}
	}
	fclose(f_ptr);
	return 0;
}

/****************************************************************************************************
*
* @name FileRemove
* @brief Delete a file in the server
*
* This function deletes mentioned file in the server. 
*
* @param sockect instance
* @param name - original file name 
* @param clientaddr - of type struct sockaddr_in
* @param clientlen 
*
* @return 0 on No error
*
****************************************************************************************************/
int FileRemove(int sockfd, char *name, struct sockaddr_in clientaddr, int clientlen)
{
        int n;
        FILE *file_ptr;
	message msg;
	int val = -10;
	/* If deletion is successful, make value to be 5. Otherwise, it will be -10 */
	if(remove(name)==0)
		val = 5;	
	sprintf(msg.buff, "%d", val);
	msg.ACK = 0;
	unsigned int rep = 0;
	/* Until the acknowledgement is received or data is sent 3 times */
	while(msg.ACK == 0 && rep < 3)
	{
		msg.ACK = 0;
		n = sendto(sockfd, &msg, MSGSIZE, 0,(struct sockaddr *)&clientaddr, clientlen);
        	n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
		rep++;
	}
	return 0;
}

/****************************************************************************************************
*
* @name FileList
* @brief List the files of a given directory
*
* This function lists out the names of all the files present in a directory . 
*
* @param sockect instance
* @param clientaddr - of type struct sockaddr_in
* @param clientlen 
*
* @return 0 on No error
*
****************************************************************************************************/
int FileList(int sockfd, struct sockaddr_in clientaddr, int clientlen)
{
	int n, count = 0;
	message msg;
	char buffer[BUFSIZE];
	bzero(&msg, MSGSIZE);
	struct dirent *de;
	DIR *dr = (opendir("."));
	msg.ACK = 0;
	/* If directory cannot be opened, send the error*/
	if(dr == NULL)
	{	strcpy(msg.buff, "Cannot Open Directory");
		while(msg.ACK == 0 && count <3)
		{
			msg.ACK = 0;
			n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
                	n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
		}
		return 1;
	}
	FILE *file_ptr = fopen("FileName", "w");
	/* Write file name to a local file */
	while ((de = readdir(dr)) != NULL)
	{
		strcat(de->d_name, "\n");
		n = fwrite(de->d_name, 1, strlen(de->d_name), file_ptr);
	}
	fclose(file_ptr);
	/* get the file size */
	file_ptr = fopen("FileName", "r");
	fseek(file_ptr, 0L, SEEK_END);
	int sz = ftell(file_ptr);
	rewind(file_ptr);
	fclose(file_ptr);
	closedir(dr);
	msg.seqno = 1;
	msg.ACK = 0;
	sprintf(msg.buff, "%d", sz);
	while(msg.ACK == 0 && count <3)
	{
		msg.ACK = 0;
		n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
		n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
		count++;
	}
	int read_size;
	bzero(&msg.buff, BUFSIZE);
        msg.ACK = 0;
	msg.seqno++;
        int rep=0;
	file_ptr = fopen("FileName", "r");
	/* send the files lists until last byte is reached */
        while((read_size=fread(&msg.buff, 1, BUFSIZE, file_ptr))>=BUFSIZE)
        {
                msg.read_bytes = read_size;
                while(msg.ACK == 0)
                {
                        msg.ACK =0;
			/* check if timeout happens. If yes, check if it is a packet drop */
                        if(rep!=0)
                                printf("Packet Dropped with SeqNo: %d\n", msg.seqno);
                        n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
                        n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
                        rep++;
                }
                rep=0;
                msg.ACK = 0;
                msg.read_bytes = 0;
                msg.seqno++;
        }
        fclose(file_ptr);
	remove("FileName");
        msg.ACK = 0;
        msg.read_bytes = read_size;
	/* Send last bytes (size will be less than 1024 bytes )*/
        while(msg.ACK == 0)
        {
                msg.ACK = 0;
                n = sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
                n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, &clientlen);
        }
	return 0;
}

/****************************************************************************************************
*
* @name Main
* @brief Implements the server functionality 
*
* This is the main function where all the server functionalities are implemented. 
*
* @param argc - argument count 
* @param **argv - pointer to argument  
*
* @return 0 on No error
*
****************************************************************************************************/
int main(int argc, char **argv) 
{
	int sockfd; /* socket */
	int portno; /* port to listen on */
	int clientlen; /* byte size of client's address */
	char buf[BUFSIZE];
	struct sockaddr_in serveraddr; /* server's addr */
	struct sockaddr_in clientaddr; /* client addr */
	struct hostent *hostp; /* client host info */
	char *hostaddrp; /* dotted decimal host addr string */
	int optval; /* flag value for setsockopt */
	int n; /* message byte size */

	/* 
	* check command line arguments 
	*/
	if (argc != 2) 
	{
    		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(1);
	}
	portno = atoi(argv[1]);

	/* 
	* socket: create the parent socket 
	*/
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) 
	error("ERROR opening socket");

	/* setsockopt: Handy debugging trick that lets 
	* us rerun the server immediately after we kill it; 
	* otherwise we have to wait about 20 secs. 
	* Eliminates "ERROR on binding: Address already in use" error. 
	*/
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv , sizeof(tv));

	/*
	* build the server's Internet address
	*/
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short)portno);

	/* 
	* bind: associate the parent socket with a port 
	*/
	if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
	error("ERROR on binding");

	/* 
	* main loop: wait for a datagram, then echo it
	*/
	clientlen = sizeof(clientaddr);
	int exit = 0;
	message msg;
	while(!exit)
	{
		/*
     		* recvfrom: receive a UDP datagram from a client
     		*/
    		bzero(&msg,MSGSIZE);
		n = 0;
		msg.ACK = 0;
		//msg.NACK = 0;
		msg.seqno = 0;
		while(n < 1)
    			n = recvfrom(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *) &clientaddr, &clientlen);

    		/* 
    		 * gethostbyaddr: determine who sent the datagram
    		 */
    		hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, 
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
    		if (hostp == NULL)
      			error("ERROR on gethostbyaddr");
    		hostaddrp = inet_ntoa(clientaddr.sin_addr);
    		if (hostaddrp == NULL)
      			error("ERROR on inet_ntoa\n");
    		printf("server received datagram from %s (%s)\n", hostp->h_name, hostaddrp);
    		
		strcpy(buf, msg.buff);
    		/* 
     		* check the command and call respective function  
     		*/
		if (strncmp(buf, "get ", 4)==0)
		{
			sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
			printf("Command received: %s\n", buf);
			memmove(buf, buf+4, strlen(buf)-4+1);
    			int result;
    			result = SendFile_Client(sockfd, buf, clientaddr, clientlen);
		}
		else if(strncmp(buf, "ls", 2)==0)
		{
			sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
			printf("command received: %s\n", buf);
			int result;
			result = FileList(sockfd, clientaddr, clientlen);
		}
		else if(strncmp(buf, "delete ", 7)==0)
		{
			sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
			printf("command received: %s\n", buf);
			memmove(buf, buf+7, strlen(buf)-7+1);
                        int result;
                        result = FileRemove(sockfd, buf, clientaddr, clientlen);
		}
		else if(strncmp(buf, "exit", 7)==0)
                {
			sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
                        printf("command received: %s\n", buf);
                        exit = 1;
                }
		else if(strncmp(buf, "put ", 4)==0)
                {
                        sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
			printf("command received: %s\n", buf);
                        memmove(buf, buf+4, strlen(buf)-4+1);
                        int result;
                        result = ReceiveFile_Client(sockfd, buf, clientaddr, clientlen);
			if(result != 0)
				printf("File not received\n");
			else
				printf("File Received\n");
                }
		else
		{
			/* If command is invalid, append INVALID COMMAND and send it back to client */
			printf("Command Invalid\n");
			msg.ACK =0;
			//msg.NACK = 0;
			msg.seqno = 0;
			strcat(buf, ": INVALID COMMAND\n");
			strcpy(msg.buff, buf);
			sendto(sockfd, &msg, MSGSIZE, 0, (struct sockaddr *)&clientaddr, clientlen);
	    	}
	}
	/* graceful exit in case 'exit' command is received */
	close(sockfd);
	printf("Exiting...........................\n");
	return 0;
}

/****************************************************************************************************
*
* End of File
*
****************************************************************************************************/
